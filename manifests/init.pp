# Install the packages necessary to run docker.
#
# $ensure: must be one of present or absent.
# Default: present
#
# $use_docker_repo: if true (the default), use docker.com's repository as
#   the source for the docker packages. For Debian this is currently the better
#   option as the version of docker in the Debian official repository is old
#   and docker changes frequently.
#
#   Note that the name of the package in the official Debian repository is
#   "docker.io" to avoid conflicting with an existing package that already
#   has the name "docker". The name of the package from docker.com is
#   "docker-ce".
# Default: true

# https://apt.dockerproject.org/repo

class package_docker (
  Enum['present', 'absent'] $ensure          = 'present',
  Boolean                   $use_docker_repo = true,
){
  if ($::osfamily == 'Debian') {
    if ($use_docker_repo) {

      # If installing, make sure we get rid of docker.io and that we have
      # the apt-transport-https package.
      if ($ensure == 'present') {
        package { 'docker.io':
          ensure => absent,
          before => Package['docker-ce'],
        }
        package { 'apt-transport-https':
          ensure => present,
          before => File['/etc/apt/sources.list.d/docker.list'],
        }
      }

      file {'/etc/apt/sources.list.d/docker.list':
        ensure  => $ensure,
        content => template('package_docker/etc/apt/sources.list.d/docker.list.erb'),
      }

      if ($ensure == 'present') {

        file {'/usr/local/bin/docker_apt_key_add':
          ensure  => $ensure,
          mode    => '0755',
          group   => 'root',
          owner   => 'root',
          content => template('package_docker/usr/local/bin/docker_apt_key_add.erb'),
          before  => Exec['docker_apt_key_add'],
        }

        exec { 'docker_apt_key_add':
          path    => '/bin:/usr/local/bin:/usr/bin:/usr/sbin',
          command => 'docker_apt_key_add',
          onlyif  => 'test ! -f /etc/apt/trusted.gpg.d/docker.gpg',
          require => [
                      File['/usr/local/bin/docker_apt_key_add'],
                      File['/etc/apt/sources.list.d/docker.list'],
                     ],
          before  => Package['docker-ce'],
        }
      } else {
        file { '/etc/apt/trusted.gpg.d/docker.gpg':
          ensure => absent,
        }
      }

      # Install or uninstall the package
      package { 'docker-ce':
        ensure  => $ensure,
      }
    } else {
      # Using official Debian packages
      package { 'docker.io':
        ensure => $ensure,
      }

      if ($ensure == 'present') {
        package { 'docker-ensure':
          ensure => absent,
        }
      }
    }
  } else {
    crit("do not yet know how to handle ${::osfamily}")
  }

}
