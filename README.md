The package_docker class installs (or removes) the docker
engine. Currently, it supports only Debian. By default it will install the
docker-engine from the dockerproject.org, but that can be overridden so
that the (older) docker package 'docker.io' from the official Debian
package repository is used instead.
